from nose.tools import assert_equals
from source_field_reader import *
test_string = "\x01\xf0\x02\x39\x0d\xe0\x39\x00\x10\x03\x00\x00\x00\x00\x00\x00\x08\x00\x02\x03\xd3\x37"
test_string2 = "\x01\xf0\x02\x39\x0d\xe0\x1e\x80\x02\x00\x00\x00\x00\x00\x00\x10\x10\x00\x10\x04\x03\x4d\x11"

def test_read_line_number():
    assert_equals(read_line_number(0x0, 0x0), 0)

def test_read_vehicle_service_number():
    assert_equals(read_vehicle_service_number(0xFF, 0x01), 7)

def test_read_over_loop():
    assert_equals(read_over_loop(0b10000001), 1)

def test_read_over_loop2():
    assert_equals(read_over_loop(0b00001001), 0)

def test_read_loop_number():
    assert_equals(read_loop_number(0b10000101), 5)

def test_reading_source_field():
    v1 = read_source_field(bytearray(test_string))
    assert_equals(v1.line_number, 57)
    assert_equals(v1.vehicle_service_number, 12)
    assert_equals(v1.over_loop, 0)
    assert_equals(v1.loop_number, 2)

def test_reading_source_field2():
    v1 = read_source_field(bytearray(test_string2))
    assert_equals(v1.line_number, 30)
    assert_equals(v1.vehicle_service_number, 10)
    assert_equals(v1.over_loop, 0)
    assert_equals(v1.loop_number, 4)
