from vehicle_detection import VehicleDetection

DLE = 0x10

""" Return a VetagCommunicator
This method reads a sourcefield as described in VCU-protocol documentation
"""
def read_source_field(message):
    escapes = 0
    prev_dle = False
    detection = VehicleDetection()
    for index, value in enumerate(message):
        if value == DLE and not prev_dle:
            escapes += 1
            prev_dle = True
            continue

        prev_dle = False
        next_escape = 0
        if message[index + 1] == DLE:
            next_escape = 1
        if index == 6 + escapes:
            detection.line_number = read_line_number(value, message[index+1+next_escape])
        if index == 7 + escapes:
            detection.vehicle_service_number = read_vehicle_service_number(value, message[index+1+next_escape])
        if index == 17 + escapes:
            detection.over_loop = read_over_loop(value)
            detection.loop_number = read_loop_number(value)
            break
    return detection

def read_line_number(b1, b2):
    return b1 | ((0x2F & b2) << 8)

def read_vehicle_service_number(b1, b2):
    return b1>>6 | b2<<2

def read_over_loop(b):
    return b>>7

def read_loop_number(b):
    return b & 0x0F
