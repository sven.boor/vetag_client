import json

class VehicleDetection:
    def __init__(self):
        self.line_number = -1
        self.vehicle_service_number = -1
        self.over_loop = -1
        self.loop_number = -1

    def getMessage(self):
        return json.dumps(vars(self))

    def toString(self):
        return ("Line: %s Service_number %s Loop: %s Overloop: %s"
        % (self.line_number, self.vehicle_service_number, self.loop_number, self.over_loop))
