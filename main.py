import vetag_communicator
import time
import os
import paho.mqtt.client as mqtt

port = ['usb-FTDI_USB2-H-1008-M_FTZ2X30T9V-if00-port0',
        'usb-FTDI_USB2-H-1008-M_FTZ2X30T9V-if01-port0',
        'usb-FTDI_USB2-H-1008-M_FTZ2X30T9V-if02-port0',
        'usb-FTDI_USB2-H-1008-M_FTZ2X30T9V-if03-port0',
        'usb-FTDI_USB2-H-1008-M_FT92X30TBN-if00-port0',
        'usb-FTDI_USB2-H-1008-M_FT92X30TBN-if01-port0',
        'usb-FTDI_USB2-H-1008-M_FT92X30TBN-if02-port0',
        'usb-FTDI_USB2-H-1008-M_FT92X30TBN-if03-port0']

def main(client):
    for index, p in enumerate(port):
        if index == 0:
            continue
        thread = vetag_communicator.vetagCommunicator(p, index, client)
        thread.start()
        time.sleep(0.11)


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

client = mqtt.Client(client_id=os.getenv("MQTT_USERNAME"))
client.on_connect = on_connect

client.tls_set()
client.username_pw_set(os.getenv("MQTT_USERNAME"), password=os.getenv("MQTT_PASSWORD"))
client.connect(os.getenv("MQTT_HOST"), 8883, 60)

if __name__ == '__main__':
    main(client)
    client.loop_forever()
