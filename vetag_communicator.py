import paho.mqtt.publish as publish
import serial
import source_field_reader
import logging
import logging.config
import threading
import os

# Frames used for communication with VTU
EOT_frame = "\x00\x04"
POLL_frame = "\x50\xF0\x05"
SELECT_frame = "\x53\xF0\x05"
ACK_frame = "\xF0\x06"

# Initialise logging
logger = logging.getLogger("vetagCommunicator")
class vetagCommunicator(threading.Thread):
    def __init__(self, port, id, mqtt_client):
        threading.Thread.__init__(self)
        self.port = port
        self.id = id
        self.mqtt_client = mqtt_client 

    def run(self):
        logger.info("Starting vetag communicator %s" % self.id)
        self.ser = serial.Serial("/dev/serial/by-id/" + self.port, 1200, timeout=1)
        self.loop()


    def read_frame(self, frame):
        if frame == EOT_frame:
            return "EOT"
        elif frame.startswith("\x01\xF0\x02"):
            self.info_frame(frame)
            self.send_ack()
            return "INFO"
        logger.info("Invalid frame %s %d" % (frame.encode('hex'), self.id))
        self.ser.write(EOT_frame)

    # This frame sends ACK, if not ACK is received EOT frame is send in read_frame.
    def send_ack(self):
        logger.info("Send ACK")
        self.ser.write(ACK_frame)
        data = self.ser.read(2)
        self.read_frame(data)

    def info_frame(self, frame):
        byte_frame = bytearray(frame)
        result = source_field_reader.read_source_field(byte_frame)
        logger.info("Sending %s" % result.getMessage())
        try:
            self.mqtt_client.publish(("/vetag_detection/%s/%d" % (os.environ.get("STOPPLACECODE"), self.id)), result.getMessage(), qos=2)
        except IOError as e:
            logger.warning("Sending data to server not succesfull error: %s %s" % (e.errno, e.strerror))
        except Exception as e:
            logger.warning("Another exception %s" % e)


    def loop(self):
        while True:
            self.ser.write(POLL_frame)
            data = self.ser.read(50)
            self.read_frame(data)


